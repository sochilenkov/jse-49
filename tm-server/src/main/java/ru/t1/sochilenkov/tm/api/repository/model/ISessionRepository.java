package ru.t1.sochilenkov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    boolean existsById(@NotNull String id);

    void remove(@NotNull Session model);

}
